from django.conf.urls import url
from .views import home_action, login_action, authenticate_action, logout_action, statistics_action

urlpatterns = [
    url(r'^$', home_action, name="index"),
    url(r'^login$', login_action, name="login"),
    url(r'^authenticate$', authenticate_action, name="authenticate"),
    url(r'^logout', logout_action, name="logout"),
    url(r'^statistics', statistics_action, name="statistics"),
]
