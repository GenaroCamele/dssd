import React from 'react';

export default class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let productDetail = null;
        if (this.props.selectedProduct) {
            // Para los empleados logueados es al precio de costo
            const initialPrice = isAuthenticated ? this.props.selectedProduct.cost_price : this.props.selectedProduct.sale_price

            productDetail = (
                <div className="panel panel-success">
                    <div className="panel-heading">
                        <strong>{this.props.selectedProduct.name}</strong>

                        {/* Informa que se esta obteniendo descuento general */}
                        {this.props.gettingGeneralDiscount &&
                            <span className="margin-left">Obteniendo descuento general</span>
                        }

                        {/* Informa que se esta obteniendo descuento de cupones */}
                        {this.props.gettingCouponDiscount &&
                            <span className="margin-left">Obteniendo descuento de cupones</span>
                        }
                    </div>
                    <div className="panel-body">
                        <div className="col-md-12 margin-bottom">
                            <div className="col-md-3">
                                <input type="text" 
                                    className="form-control"
                                    value={this.props.couponInput}
                                    onChange={this.props.changeCouponInput}
                                    disabled={isAuthenticated}
                                    pattern="[0-9]*"
                                />
                            </div>
                            <div className="col-md-2">
                                <button className="btn btn-warning"
                                    disabled={!this.props.couponInput || this.props.gettingCouponDiscount}
                                    onClick={this.props.getCouponDiscount}>
                                        Validar cupon
                                </button>
                            </div>
                        </div>
                        
                        <div className="col-md-12">
                            <p>
                                Precio: <strong>${initialPrice}</strong>
                            </p>

                            <p>
                                Descuento general: <strong className="discount-span">- ${this.props.generalDiscount}</strong>
                            </p>

                            <p>
                                Descuento por cupón: <strong className="discount-span">- ${this.props.couponDiscount}</strong>
                            </p>

                            <p>
                                Precio final: <strong className="green">${initialPrice - this.props.generalDiscount - this.props.couponDiscount}</strong>
                            </p>
                            <button className="btn btn-success pull-right margin-left" onClick={this.props.buy} disabled={this.props.buying}>Confirmar compra</button>
                            <button className="btn btn-danger pull-right" onClick={this.props.clearProduct}>Cancelar</button>
                        </div>
                    </div>
                </div>
            );
        } else {
            productDetail = (
                <div className="col-md-12 text-center">
                    <h5 className="text-muted">No hay producto seleccionado</h5>
                </div>
            );
        }

        return (
            <div>
                <h2 id="product-detail-header">Detalle de la compra</h2>
                { productDetail }
            </div>
        );
    }
}