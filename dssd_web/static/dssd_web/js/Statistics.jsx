import React from "react";
import ReactDOM from "react-dom";

// Estilos
import '../css/statistics.css'

// Componentes
import Navbar from './Navbar.jsx';
import StatisticsPanel from './StatisticsPanel.jsx';

class Statistics extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
            <div>
                {/* Navbar general */}
                <Navbar />

                {/* Panel con los graficos estadisticos */}
                <StatisticsPanel/>
            </div>
		);
	}
}

ReactDOM.render(<Statistics />, document.getElementById("app-statistics"));