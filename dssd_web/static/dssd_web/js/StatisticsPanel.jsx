import React from 'react';

// Graficos
import { Pie } from 'react-chartjs';

export default class StatisticsPanel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            pieByCouponsData: [],
            pieByUsersData: []
        };

        // Configuracion de los graficos
        this.pieSettings = {
            //Number - Amount of animation steps
            animationSteps: 100,

            //String - Animation easing effect
            animationEasing: "easeOutBounce",

            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,

            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
        };
    }

    /**
     * Cuando se termina de montar el componente, cargo los datos
     */
    componentDidMount() {
        this.getPieByCouponsData();
        this.getPieByUsersData();
    }

    /**
     * Obtiene los datos desde el servidor
     */
    getPieByCouponsData() {
        let self = this;

        // Obtengo las ventas sin cupones
        $.ajax({
            url: listSalesURL + '?coupon__isnull=True',
        }).done(function (withoutJSON) {
            // Obtengo las venas con cupones
            $.ajax({
                url: listSalesURL + '?coupon__isnull=False',
            }).done(function (withJSON) {
                self.setState({ pieByCouponsData: [
                    {
                        value: withoutJSON.count,
                        color: "#F7464A",
                        highlight: "#FF5A5E",
                        label: "Sin cupones"
                    },
                    {
                        value: withJSON.count,
                        color: "#1d890a",
                        highlight: "#1d9308",
                        label: "Con cupones"
                    }
                ]});
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert('Error al obtener las ventas por cupones. Intente nuevamente mas tarde')
                console.log('Error la obtener el descuento general -> ', jqXHR, textStatus, errorThrown);
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert('Error al obtener las ventas por cupones. Intente nuevamente mas tarde')
            console.log('Error la obtener el descuento general -> ', jqXHR, textStatus, errorThrown);
        });
    }

    /**
     * Obtiene los datos desde el servidor
     */
    getPieByUsersData() {
        let self = this;

        // Obtengo las ventas sin cupones
        $.ajax({
            url: listSalesURL + '?user_was_logged=False',
        }).done(function (withoutJSON) {
            // Obtengo las venas con cupones
            $.ajax({
                url: listSalesURL + '?user_was_logged=True',
            }).done(function (withJSON) {
                self.setState({ pieByUsersData: [
                    {
                        value: withoutJSON.count,
                        color: "#F7464A",
                        highlight: "#FF5A5E",
                        label: "No logueados"
                    },
                    {
                        value: withJSON.count,
                        color: "#1d890a",
                        highlight: "#1d9308",
                        label: "Logueados"
                    }
                ]});
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert('Error al obtener las ventas por usuarios. Intente nuevamente mas tarde')
                console.log('Error la obtener el descuento general -> ', jqXHR, textStatus, errorThrown);
            });
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert('Error al obtener las ventas por usuarios. Intente nuevamente mas tarde')
            console.log('Error la obtener el descuento general -> ', jqXHR, textStatus, errorThrown);
        });

    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h2>Estadísticas</h2>
                    </div>
                </div>

                <div className="row">    
                    <div className="col-md-6 text-center">
                        <h2>Ventas por cupones</h2>
                        <Pie data={this.state.pieByCouponsData} options={this.pieSettings} width="600" height="250"/>
                    </div>

                    <div className="col-md-6 text-center">
                        <h2>Ventas por usuarios</h2>
                        <Pie data={this.state.pieByUsersData} options={this.pieSettings} width="600" height="250" />
                    </div>
                </div>
            </div>
        );
    }
}