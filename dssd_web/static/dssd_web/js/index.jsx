import React from "react";
import ReactDOM from "react-dom";

// Estilos
import '../css/index.css'

// Componentes
import Navbar from './Navbar.jsx';
import ProductsManager from './ProductsManager.jsx';

class Index extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div>
				<Navbar/>
				<div className="row">
					<ProductsManager/>
				</div>
			</div>
		);
	}
}

ReactDOM.render(<Index/>, document.getElementById("app"));