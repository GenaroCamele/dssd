import React from 'react';

export default function Navbar() {
    // Boton para iniciar o cerrar sesion
    const button = !isAuthenticated
        ? <a href={loginURL}>Iniciar sesión</a>
        : <a href={logoutURL}>Salir</a>;

    return (
        <nav className="navbar navbar-default">
            <div className="container-fluid">
                <div className="navbar-header">
                    <a className="navbar-brand" href="#">ChangoMás</a>
                </div>

                {/* Barra de links */}
                <ul className="nav navbar-nav">
                    {/* Link de menu de inicio */}
                    <li><a href={indexURL}>Inicio</a></li>

                    {/* Link de graficos estadisticos */}
                    {statisticsURL && 
                        <li><a href={statisticsURL}>Estadísticas</a></li>
                    }
                </ul>

                <p id="login-button" className="navbar-text navbar-right">
                    { button }
                </p>
            </div>
        </nav>
    );
}