import React from 'react';

// Componentes
import ProductDetail from './ProductDetail.jsx';

export default class ProductsManager extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            bonita: true,
            products: [],
            buying: false,
            gettingGeneralDiscount: false,
            selectedProduct: null,
            generalDiscount: 0,
            couponInput: '',
            gettingCouponDiscount: false,
            couponDiscount: 0,
            loading: false,
            error: false
        };

        // Bindeo 'this' a los metodos llamados desde la vista
        this.clearProduct = this.clearProduct.bind(this);
        this.changeCouponInput = this.changeCouponInput.bind(this);
        this.getCouponDiscount = this.getCouponDiscount.bind(this);
        this.buy = this.buy.bind(this);
        this.toggleCheckbox = this.toggleCheckbox.bind(this);
    }

    /**
     * Funcion que se ejecuta cuando termina de renderizar el componente
     */
    componentDidMount() {
        this.getProducts();
    }

    /**
     * Cambia el valor para saber contra que chocar
     */
    toggleCheckbox() {
        this.setState(prevState => ({ bonita: !prevState.bonita }));
    }

    /**
     * Devuelve el valor de una cookie
     * @param {string} cname Nombre del cookie a obtener
     */
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    /**
     * Obtiene via AJAX los productos
     */
    getProducts() {
        let self = this;

        self.setState({ loading: true }, () => {
            $.ajax({
                url: getProductsURL
            }).done(function (productsJSON) {
                if (productsJSON && productsJSON.results !== undefined) {
                    self.setState({ products: productsJSON.results });
                } else {
                    self.setState({ error: true });
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log('Error la obtener los productos -> ', jqXHR, textStatus, errorThrown);
                self.setState({ error: true });
            }).always(function () {
                self.setState({ loading: false });
            });
        });
    }

    buy() {
        if (this.state.bonita) {
            this.buyToBonita();
        } else {
            this.buyToBackend();
        }
    }

    /**
     * Chequea el estado de un caso en particular para seguir el estado del mismo
     * @param {number} caseId Id del caso a chequear el estado
     */
    checkStatusCase(caseId) {
        let self = this;
        
        // Intenta 5 veces obtener el estado del caso
        $.ajax({
            url: 'http://127.0.0.1:8080/bonita/API/bpm/case/' + caseId,
            xhrFields: { withCredentials: true },
            retryLimit: 5
        }).done(function (caseObj) {
            switch (caseObj.state) {
                // En caso de haberse completado informo y limpio todo
                case 'completed':
                case 'started':
                    alert('Compra realizada con exito');
                    self.setState({ buying: false });
                    self.clearProduct();
                break;
                // Caso de error, informo
                case 'suspended':
                case 'cancelled':
                case 'aborted':
                case 'error':
                case 'aborting':
                    self.alertBuyingError();
                    console.log('Error al comprar. Case Obj -> ', caseObj);
                // En cualquiera de los otros estados, reintento
                default:
                    if (this.retryLimit--) {
                        //try again
                        $.ajax(this);
                        return;
                    }
                break;
            }
        }).fail(function () {
            if (this.retryLimit--) {
                //try again
                $.ajax(this);
                return;
            }
        });        
    }

    /**
     * Corre un caso del proceso de Bonita con el id pasado por parametro
     * @param {number} processId Id del caso de Bonita
     */
    runCase(processId) {
        let self = this;

        $.ajax({
            url: 'http://127.0.0.1:8080/bonita/API/bpm/process/' + processId + '/instantiation',
            type: "POST",
            contentType: "application/json",
            /*passing the X-Bonita-API-Token for the CSRF security filter*/
            headers: { 'X-Bonita-API-Token': self.getCookie('X-Bonita-API-Token') },
            data: JSON.stringify({
                product_id: self.state.selectedProduct.id,
                coupon_number: self.state.couponInput ? self.state.couponInput : 0,
                logged: isAuthenticated,
                session: cookieSessionId
            }),
            xhrFields: { withCredentials: true },
        }).done(function (caseObj, textStatus, xhr) {
            if (xhr.status == 200) {
                self.checkStatusCase(caseObj.caseId);
            } else {
                self.alertBuyingError();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.alertBuyingError();
            console.log('Error al comprar -> ', jqXHR, textStatus, errorThrown);
        }).always(function () {
            self.setState({ buying: false });
        });
    }

    /**
     * Obtiene el ID del proceso de bonita para poder crear un caso
     */
    getProcessId() {
        let self = this;

        $.ajax({
            url: 'http://127.0.0.1:8080/bonita/API/bpm/process?s=proceso',
            xhrFields: {
                withCredentials: true
            }
        }).done(function (resJSON) {
            const firstProcess = resJSON[0];
            if (firstProcess && firstProcess.id) {
                self.runCase(firstProcess.id)
            } else {
                self.alertBuyingError();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.alertBuyingError();
            console.log('Error al comprar -> ', jqXHR, textStatus, errorThrown);
        });
    }

    alertBuyingError() {
        this.setState({ buying: false });
        alert('Ocurrió un error al realizar la compra. Intente nuevamente más tarde');
    }

    /**
     * Confirma la compra contra Bonita
     */
    buyToBonita() {
        let self = this;

        self.setState({ buying: true }, () => {
            // Me logueo
            $.ajax({
                url: 'http://127.0.0.1:8080/bonita/loginservice',
                type: 'POST',
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    username: 'walter.bates',
                    password: 'bpm',
                    redirect: false
                }
            }).done(function () {
                // Estoy logueado, obtengo el ID del proceso
                self.getProcessId();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                self.alertBuyingError();
                console.log('Error al comprar -> ', jqXHR, textStatus, errorThrown);
            });
        });
    }

    /**
     * Confirma la compra contra el backend en Django
     */
    buyToBackend() {
        let self = this;

        self.setState({ buying: true }, () => {
            $.ajax({
                url: buyURL,
                type: 'POST',
                data: {
                    product_id: self.state.selectedProduct.id,
                    coupon_number: self.state.couponInput
                }
            }).done(function (resJSON) {
                if (resJSON && resJSON.length && resJSON[0].pk) {
                    // Informo y limpio todo
                    alert('Compra realizada con exito');
                    self.clearProduct();
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert('Ocurrió un error al realizar la compra. Intente nuevamente más tarde');
                console.log('Error al comprar -> ', jqXHR, textStatus, errorThrown);
            }).always(function () {
                self.setState({ buying: false });
            });
        });
    }

    /**
     * Obtiene el descuento general para el producto seleccionado
     */
    getGeneralDiscount() {
        let self = this;

        self.setState({ gettingGeneralDiscount: true }, () => {
            $.ajax({
                url: getGeneralDicountURL,
                type: 'POST',
                data: {
                    product_id: self.state.selectedProduct.id
                }
            }).done(function (resJSON) {
                if (resJSON && resJSON.discount !== undefined) {
                    self.setState({ generalDiscount: resJSON.discount });
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log('Error la obtener el descuento general -> ', jqXHR, textStatus, errorThrown);
            }).always(function () {
                self.setState({ gettingGeneralDiscount: false });
            });
        });
    }

    /**
     * Obtiene el descuento de cupon para el producto seleccionado
     */
    getCouponDiscount() {
        let self = this;

        self.setState({ gettingCouponDiscount: true }, () => {
            $.ajax({
                url: getCouponDicountURL,
                type: 'POST',
                data: {
                    product_id: self.state.selectedProduct.id,
                    coupon_number: self.state.couponInput
                }
            }).done(function (resJSON) {
                if (resJSON && resJSON.discount !== undefined) {
                    self.setState({ couponDiscount: resJSON.discount });
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log('Error la obtener el descuento general -> ', jqXHR, textStatus, errorThrown);
            }).always(function () {
                self.setState({ gettingCouponDiscount: false });
            });
        });
    }

    /**
     * Maneja el cambio en el input del cupon
     * @param {Event} e Evento de cambio del input
     */
    changeCouponInput(e) {
        // Si no es valido no hago nada
        if (!e.target.validity.valid) {
            return;
        }

        this.setState({ couponInput: e.target.value });
    } 

    /**
     * Selecciona un producto para mostrar los detalles
     * @param {*} product Producto seleccionado
     */
    selectProduct(product) {
        this.setState({selectedProduct: product }, () => {
            // Si no esta logueado, se fija los descuentos
            if (!isAuthenticated) {
                this.getGeneralDiscount();
            }
        });
    }
    
    /**
     * Limpia el producto seleccionado y su descuento
     */
    clearProduct() {
        this.setState({
            selectedProduct: null,
            couponInput: '',
            generalDiscount: 0,
            couponDiscount: 0
        });
    }

    render() {
        const productList = this.state.products.map((product) => {
            // Para los empleados logueados es al precio de costo
            const finalPrice = isAuthenticated ? product.cost_price : product.sale_price

            return (
                <div key={product.id.toString()} className="col-md-3">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            {product.name}
                            <span className="pull-right cursor-pointer glyphicon glyphicon-ok green-hover animated-ease-1seg"
                                aria-hidden="true"
                                title="Seleccionar producto"
                                onClick={() => this.selectProduct(product)}
                            ></span>
                        </div>
                        <div className="panel-body">
                            <p>
                                Precio: <strong>${finalPrice}</strong>
                            </p>
                        </div>
                    </div>
                </div>
            );
        });

        return (
            <div>
                {/* PAra chocar contra Djando o Bonita */}
                <div className="col-md-12 text-center margin-bottom">
                    <input id="is-bonita" type="checkbox" checked={this.state.bonita} onChange={this.toggleCheckbox} />
                    <label htmlFor="is-bonita">Bonita</label>
                </div>

                {/* Listado de productos para seleccionar */}
                <div className="col-md-6">
                    { productList }
                </div>

                {/* Resumen con los detalles del producto seleccionado para comprar */}
                <div className="col-md-6">
                    <ProductDetail
                        selectedProduct={this.state.selectedProduct}
                        generalDiscount={this.state.generalDiscount}
                        couponDiscount={this.state.couponDiscount}
                        gettingGeneralDiscount={this.state.gettingGeneralDiscount}
                        gettingCouponDiscount={this.state.gettingCouponDiscount}
                        clearProduct={this.clearProduct}
                        couponInput={this.state.couponInput}
                        changeCouponInput={this.changeCouponInput}
                        getCouponDiscount={this.getCouponDiscount}
                        buy={this.buy}
                        buying={this.state.buying}
                    />
                </div>
            </div>
        );
    }
}