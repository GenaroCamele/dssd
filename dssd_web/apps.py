from django.apps import AppConfig


class DssdWebConfig(AppConfig):
    name = 'dssd_web'
