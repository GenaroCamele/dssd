from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def home_action(request):
    """Renderiza el template del index"""
    return render(request, 'dssd_web/index.html')


@login_required
def statistics_action(request):
    """Renderiza el template de los graficos estadisticos"""
    return render(request, 'dssd_web/statistics.html')


def login_action(request):
    """Rederiza el template del login"""
    # Si esta logueado redirige al index
    if request.user.is_authenticated:
        return redirect('index')

    return render(request, 'dssd_web/login.html')


def authenticate_action(request):
    """Valida la autenticacion de un usuario"""
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('index')
    else:
        return render(request, 'dssd_web/login.html', {
            'error_message': 'Usuario o password inválida'
        })


def logout_action(request):
    """Cierra la sesion de un usuario"""
    logout(request)
    return render(request, 'dssd_web/login.html')
