from rest_framework import generics, filters
from django_filters.rest_framework import DjangoFilterBackend
from .serializer import *


# Create your views here.
class CouponList(generics.ListCreateAPIView):
    """Habilita el GET y POST para listar y agregar cupones"""
    queryset = Coupon.objects.all()
    serializer_class = CouponSerializer

    filter_backends = (
        DjangoFilterBackend,  # Agregamos el filtro
        filters.SearchFilter,  # Agregamos la busqueda
        filters.OrderingFilter  # Agregamos el ordenamiento
    )
    filter_fields = '__all__'  # Campos por lo que se puede filtrar

    search_fields = (  # Campos por los que se puede buscar
        'used',
    )


class CouponDetail(generics.RetrieveUpdateDestroyAPIView):
    """Habilita el PATCH y el DELETE para editar y eliminar cupones"""
    queryset = Coupon.objects.all()
    serializer_class = CouponSerializer
