from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from dssd_coupon.views import *

app_name = "dssd_coupon"
urlpatterns = [
    # URLs de los cupones
    url(r'^$', CouponList.as_view()),
    url(r'^(?P<pk>[0-9]+)/$', CouponDetail.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
