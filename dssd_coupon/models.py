from django.db import models


class Coupon(models.Model):
    number = models.IntegerField(unique=True)
    used = models.BooleanField()
