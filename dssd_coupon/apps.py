from django.apps import AppConfig


class DssdCouponConfig(AppConfig):
    name = 'dssd_coupon'
