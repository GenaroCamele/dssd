from django.conf.urls import url
from .views import EmployeeList, EmployeeDetail, EmployeeTypeList, EmployeeTypeDetail

# Agregamos las URLs
urlpatterns = [
    url(r'^employees/$', EmployeeList.as_view()),
    url(r'^employees/(?P<pk>[0-9]+)/$', EmployeeDetail.as_view()),
    url(r'^employee-types/$', EmployeeTypeList.as_view()),
    url(r'^employee-types/(?P<pk>[0-9]+)/$', EmployeeTypeDetail.as_view()),
]
