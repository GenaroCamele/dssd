from .models import Employee, EmployeeType
from rest_framework import serializers

# Empleados


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ('id', 'first_name', 'surname', 'email', 'employee_type')  # Ocultamos contraseña


# Tipo de empleados


class EmployeeTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeType
        fields = '__all__'  # Mostramos todos los campos
