from .models import Employee, EmployeeType
from .serializers import EmployeeSerializer, EmployeeTypeSerializer
from rest_framework import generics, filters
from django_filters.rest_framework import DjangoFilterBackend

# Registramos las vistas de las APIs que van a permitir
# utilizar los metodos GET, POST, PATCH y DELETE para
# los modelos


# Empleados

class EmployeeList(generics.ListCreateAPIView):
    """Habilita el GET y POST para listar y agregar empleados"""
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer  # Serializador
    filter_backends = (
        DjangoFilterBackend,  # Agregamos el filtro
        filters.SearchFilter,  # Agregamos la busqueda
        filters.OrderingFilter  # Agregamos el ordenamiento
    )
    filter_fields = (  # Campos por lo que se puede filtrar
        'first_name',
        'surname',
        'email',
        'employee_type'
    )
    search_fields = (  # Campos por los que se puede buscar
        'first_name',
        'surname',
        'email',
        'employee_type__initials'  # Podemos buscar por campos de modelos foraneos tambien
    )


class EmployeeDetail(generics.RetrieveUpdateDestroyAPIView):
    """Habilita el PATCH y el DELETE para editar y eliminar empleados"""
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


# Tipo de empleados


class EmployeeTypeList(generics.ListCreateAPIView):
    """Habilita el GET y POST para listar y agregar tipos de empleado"""
    queryset = EmployeeType.objects.all()
    serializer_class = EmployeeTypeSerializer


class EmployeeTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    """Habilita el PATCH y el DELETE para editar y eliminar tipos de empleado"""
    queryset = EmployeeType.objects.all()
    serializer_class = EmployeeTypeSerializer
    filter_backends = (filters.OrderingFilter,)  # Agregamos el ordenamiento

