from django.contrib.auth.models import User
from django.db import models


class EmployeeType(models.Model):
    """Entidad Tipo de empleado"""
    initials = models.CharField(max_length=10)
    description = models.CharField(max_length=50)

    def __str__(self):
        return self.initials


class Employee(models.Model):
    """Entidad Empleado"""
    first_name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    email = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=50)
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)  # Relacion al usuario que se loguea
    employee_type = models.ForeignKey(EmployeeType, null=True, on_delete=models.SET_NULL)
