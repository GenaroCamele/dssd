from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/stock/', include('dssd_app.urls')),
    path('api/rrhh/', include('dssd_rrhh.urls')),
    path('api/coupons/', include('dssd_coupon.urls')),
    path('web/', include('dssd_web.urls')),
]

urlpatterns += [
    path('api/v1/auth', include('rest_framework.urls', namespace='rest_framework'))
]
