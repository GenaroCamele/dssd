const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
var webpack = require('webpack');

// Variables globales propias
const outputPath = './dssd_web/static/dssd_web/bundles/';
const staticFilesPath = './dssd_web/static/dssd_web/';

module.exports = {
		entry: {
			base: staticFilesPath + 'js/base.jsx',
			index: staticFilesPath + 'js/index.jsx',
			statistics: staticFilesPath + 'js/Statistics.jsx',
		}, // entry point of our app. assets/js/index.js should require other js modules and dependencies it needs
		output: {
			path: path.resolve(outputPath),
			filename: "[name].js",
		},
		module: {
				rules: [
					{
						test: /\.(js|jsx)$/,
						exclude: /node_modules/,
						use: ['babel-loader']
					},
					{
						test: /\.(png|jpe?g|gif|svg|ico|css)$/,
						use: [
							{
								loader: 'file-loader',
								options: {
									name: '[name].[ext]',
								}
							},
							"extract-loader",
							'css-loader'
						]
					}
				]
		},
		resolve: {
			extensions: ['*', '.js', '.jsx', '.css']
		},
		plugins: [
			new webpack.ProvidePlugin({
				$: 'jquery',
				jQuery: 'jquery'
			}),
			new CleanWebpackPlugin([outputPath], {}),
		]
};