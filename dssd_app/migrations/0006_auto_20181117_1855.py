# Generated by Django 2.1.1 on 2018-11-17 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dssd_app', '0005_merge_20181021_2031'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('final_price', models.FloatField()),
                ('discounted_amount', models.FloatField()),
                ('user_was_logged', models.BooleanField()),
            ],
        ),
        migrations.AlterField(
            model_name='product',
            name='cost_price',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='product',
            name='sale_price',
            field=models.FloatField(),
        ),
        migrations.AddField(
            model_name='sale',
            name='product',
            field=models.ManyToManyField(to='dssd_app.Product'),
        ),
    ]
