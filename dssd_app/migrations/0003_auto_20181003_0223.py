# Generated by Django 2.1.1 on 2018-10-03 02:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dssd_app', '0002_auto_20180928_2332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
