# Generated by Django 2.1.1 on 2018-11-17 21:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dssd_coupon', '0002_auto_20181021_2025'),
        ('dssd_app', '0007_auto_20181117_1911'),
    ]

    operations = [
        migrations.AddField(
            model_name='sale',
            name='coupon',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='dssd_coupon.Coupon'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dssd_app.Product'),
        ),
    ]
