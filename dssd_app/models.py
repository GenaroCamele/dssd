from django.db import models
from dssd_coupon.models import Coupon


class ProductType(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255, blank=True, unique=True)
    cost_price = models.FloatField( blank=True)
    sale_price = models.FloatField( blank=True)
    stock = models.IntegerField( blank=True)
    maximum_stock = models.IntegerField( blank=True)
    minimum_stock = models.IntegerField( blank=True)
    product_type = models.ForeignKey(ProductType, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class Sale(models.Model):
    product = models.ForeignKey(Product, null=False, on_delete=models.CASCADE)
    coupon = models.ForeignKey(Coupon, null=True, on_delete=models.SET_NULL)
    final_price = models.FloatField()
    discounted_amount = models.FloatField()
    user_was_logged = models.BooleanField()

