from rest_framework import generics, filters
from django_filters.rest_framework import DjangoFilterBackend
from .serializer import *
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from .models import *
from django.views.decorators.csrf import csrf_exempt
import json


# Controladores para la creacion de Productos del Stock

class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    filter_backends = (
        DjangoFilterBackend,  # Agregamos el filtro
        filters.SearchFilter,  # Agregamos la busqueda
        filters.OrderingFilter  # Agregamos el ordenamiento
    )
    filter_fields = '__all__'  # Campos por lo que se puede filtrar

    search_fields = (  # Campos por los que se puede buscar
        'name',
        'cost_price',
        'sale_price',
        'stock',
        'maximum_stock',
        'minimum_stock',
        'product_type__name'  # Podemos buscar por campos de modelos foraneos tambien
    )


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


# Controladores para la creacion de los tipos de Productos del Stock

class ProductTypeList(generics.ListCreateAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializer


class ProductTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializer

# Controladores para el listado de las ventas


class SaleList(generics.ListAPIView):
    """Habilito solo el listado de las ventas"""
    queryset = Sale.objects.all().select_related('coupon')
    serializer_class = SaleSerializer
    filter_backends = (
        DjangoFilterBackend,  # Agregamos el filtro
        # filters.SearchFilter,  # Agregamos la busqueda
        # filters.OrderingFilter  # Agregamos el ordenamiento
    )
    filter_fields = {
        'coupon': ['exact', 'isnull'],
        'product': ['exact'],
        'user_was_logged': ['exact']
    }


class SaleDetail(generics.RetrieveDestroyAPIView):
    """Habilito los datos de una venta en particular y su eliminacion"""
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer


def get_coupon_discount(product, coupon):
    """Devuelve el descuento aplicado a partir de un cupon"""
    if coupon.used:
        return 0.0

    general_discount = get_discount(product)
    price_with_discount = product.sale_price - general_discount
    return price_with_discount * 0.03


def get_discount(product):
    """Devuelve el descuento aplicado a partir del producto"""
    is_eletronic = product.product_type.name == 'Electronica'
    cost_price = product.cost_price
    sale_price = product.sale_price
    price_diff = sale_price - cost_price  # Diferencia entre precio de venta y precio de costo
    if is_eletronic:
        return price_diff * 0.5  # Resto el 50% del margen al precio de venta

    # En caso de que sea cualquier otro tipo de producto
    # Si el margen supera el 10% aplico el descuento
    if price_diff > (cost_price * 0.1):
        price_diff -= cost_price * 0.1
        return price_diff * 0.8

    # No hago ningun descuento
    return 0


@csrf_exempt
def get_coupon_discount_view(request):
    """Obtiene el id del producto y el id del cupon
    y devuelve el descuento"""
    if request.content_type == 'application/json':
        post_content = json.loads(request.body)
        product_id = post_content['product_id']  if 'product_id' in post_content else None
        coupon_number = post_content['coupon_number'] if 'coupon_number' in post_content else None
    else:
        product_id = request.POST.get('product_id')
        coupon_number = request.POST.get('coupon_number')

    if request.user.is_authenticated or not product_id or not coupon_number:
        discount = 0.0
    else:
        try:
            product = Product.objects.get(pk=product_id)
            coupon = Coupon.objects.get(number=coupon_number)
            discount = get_coupon_discount(product, coupon)
        except (Product.DoesNotExist, Coupon.DoesNotExist):
            discount = 0.0

    return JsonResponse({'discount': discount})


@csrf_exempt
def get_discount_view(request):
    """Obtiene el id del producto devuelve el descuento"""
    if request.content_type == 'application/json':
        post_content = json.loads(request.body)
        product_id = post_content['product_id']  if 'product_id' in post_content else None
    else:
        product_id = request.POST.get('product_id')

    # Si no enviaron un id de producto o estoy autenticado
    if not product_id or request.user.is_authenticated:
        discount = 0.0
    else:
        try:
            product = Product.objects.get(pk=product_id)
            discount = get_discount(product)
        except Product.DoesNotExist:
            discount = 0.0

    return JsonResponse({'discount': discount})


@csrf_exempt
def add_sale(request):
    """Define el comportamiento para agregar una venta"""

    if request.content_type == 'application/json':
        post_content = json.loads(request.body)
        product_id = post_content['product_id']  if 'product_id' in post_content else None
    else:
        product_id = request.POST.get('product_id')
    if not product_id:
        # Devuelvo una vista vacia
        sale_ans = Sale()
    else:
        # Obtengo el resto de los datos
        if request.content_type == 'application/json':
            coupon_number = post_content['coupon_number']  if 'coupon_number' in post_content else None
        else:
            coupon_number = request.POST.get('coupon_number')
            
        try:
            product = Product.objects.get(pk=product_id)
            # Obtengo el precio: si es un usuario logueado (es empleado)
            # es al costo, sino se utiliza el precio de venta
            is_authenticated = request.user.is_authenticated
            final_price = product.cost_price if is_authenticated else product.sale_price
            if not is_authenticated and coupon_number:
                try:
                    coupon = Coupon.objects.get(number=coupon_number)
                    discount_coupon = get_coupon_discount(product, coupon)

                    # Marco como usado al cupon
                    coupon.used = True
                    coupon.save()
                except Coupon.DoesNotExist:
                    # Si no hay cupon, no se aplica el descuento
                    coupon = None
                    discount_coupon = 0.0
            else:
                coupon = None
                discount_coupon = 0.0

            # Saco el descuento total y lo aplico
            general_discount = 0.0 if request.user.is_authenticated else get_discount(product)
            total_discount = general_discount + discount_coupon
            final_price -= total_discount

            # Reduzco el stock del producto
            product.stock -= 1
            product.save()

            # Guardamos la venta
            sale_ans = Sale.objects.create(
                product=product,
                coupon=coupon,
                discounted_amount=total_discount,
                final_price=final_price,
                user_was_logged=is_authenticated
            )

        except Product.DoesNotExist:
            # Devuelvo una venta vacia
            sale_ans = Sale()

    data = serializers.serialize('json', [sale_ans])
    return HttpResponse(data, content_type="application/json")
