from django.apps import AppConfig


class DssdAppConfig(AppConfig):
    name = 'dssd_app'
