from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from dssd_app.views import *

app_name = "dssd_app"

urlpatterns = [

    # URLs de los Productos de Stock
    url(r'^product/$', ProductList.as_view(), name='list_products'),
    url(r'^product/(?P<pk>[0-9]+)/$', ProductDetail.as_view(), name='details_product'),

    # URLs de los Tipos de Productos del Stock
    url(r'^product-types/$', ProductTypeList.as_view(), name='list_product_types'),
    url(r'^product-types/(?P<pk>[0-9]+)/$', ProductTypeDetail.as_view(), name='details_product_type'),

    # URLs para lo relacionado a las ventas
    url(r'^sales/$', SaleList.as_view(), name='list_sales'),
    url(r'^sales/(?P<pk>[0-9]+)/$', SaleDetail.as_view(), name='details_sale'),
    url(r'^sales/add/$', add_sale, name='add_sale'),
    url(r'^sales/get-coupon-discount/$', get_coupon_discount_view, name='get_coupon_discount'),
    url(r'^sales/get-discount/$', get_discount_view, name='get_discount')
]
urlpatterns = format_suffix_patterns(urlpatterns)
